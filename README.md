# Political style classifier

See how texts (eg. speeches) match styles from certain newspapers, based on texts from their websites.

Start the server using `python ps_server.py`. By default it will run at port 8011. (This is needed as the index page has to query the classifier)

## What does what?

1. Crawl the websites for data: `python ps_crawl.py` It will be stored in `newspaper_items.csv`.
2. Train the classifier based on the data in `newspaper_items.csv`:
    `python ps_train.py`. It is saved to `newspapers.p`.
3. To test training/classification run `python ps_classify.py`. This scripts trains the classifier, without saving it, and applies it to all files in the 'speeches' subdirectory.
4. To server out the webinterface, run `python ps_server.py`. It will run on port 8011.