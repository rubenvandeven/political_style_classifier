#!/usr/bin/env python
import os, sys

from pattern.server import App
from pattern.server import MINUTE, HOUR, DAY

from pattern.vector import SVM,Document, LEMMA
from pattern.web import plaintext

import pystache

app = App("api")

classifier = SVM.load("newspapers.p")

# @app.route("/")
# def index():
# 	return open(os.path.join(os.path.dirname(__file__),'/www/index.html')).read()

@app.route("/")
def index():
	tpl_file = os.path.join(os.path.dirname(__file__), 'index.tpl')
	speech_dir = os.path.join(os.path.dirname(__file__), 'speeches')
	with open(tpl_file) as tf:
		tpl = tf.read()
	speeches = os.listdir(speech_dir)
	return pystache.render(tpl, {'speeches':speeches, 'classes': classifier.classes})

@app.route("/newspaper", limit=100, time=HOUR, key=lambda data: app.request.ip)
def predict_newspaper(q=""):
	q = plaintext(q)
	d = Document(q, stopwords=False, stemmer=LEMMA)
	newspaper = classifier.classify(d, discrete=True)
	return {
          "newspaper": newspaper
    }

@app.route("/speech", limit=100, time=HOUR, key=lambda data: app.request.ip)
def predict_newspaper(speech=""):
	speech = open(os.path.join('speeches',speech)).read()
	return {
          "speech": speech
    }
    
# Create an account for user with key=1234 (do once).
# You can generate fairly safe keys with app.rate.key().
# if not app.rate.get(key="1234", path="/language/paid"):
#     app.rate.set(key="1234", path="/language/paid", limit=10000, time=DAY)
    
# Try it out with the key and without the key:
# http://127.0.0.1:8080/language/paid?q=hello&key=1234
# http://127.0.0.1:8080/language/paid?q=hello           (403 error)

# A rate.db SQLite database was created in the current folder.
# If you want to give it another name, use App(rate="xxx.db").
# To view the contents of the database,we use the free 
# SQLite Database Browser (http://sqlitebrowser.sourceforge.net).

# If the web service is heavily used,
# we may want to use more threads for concurrent requests
# (default is 30 threads with max 20 queueing):

app.run("0.0.0.0", port=8011, threads=100, queue=50)