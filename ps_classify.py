#!/usr/bin/env python
from pattern.web import plaintext
from pattern.vector import Document, NB, KNN, SVM, POLYNOMIAL, LEMMA, EUCLIDEAN
from pattern.db import csv

import progressbar

'''
Train a the classifier (just as ps_train.py), but rather then saving it,
immediately apply it to all files in the 'speeches' subdirectory. Used for testing.
'''

# classifier = SVM(kernel=POLYNOMIAL, degree=10, probability=True)
# SVM seems strongly biased towards the majority of input. Our input does not have
# an equal amount of articles per newspaper, so we use KNN instead (as some distances seem to 
# have less of that particular bias)
classifier = KNN()

# test a custom tagger
def instance(review):                     # "Great book!"
	from pattern.nl import tag, predicative, parsetree
	from pattern.vector import count
	v = tag(review)                       # [("Great", "JJ"), ("book", "NN"), ("!", "!")]
	# print v
	v = [word for (word, pos) in v if pos in ("JJ", "RB", "CC", "IN", "DT", "MD", "PRP") or word in ("!")]
	# v = [word for (word, pos) in v]
	# v = [parsetree(word, relations=True, lemmata=True)[0].lemma[0] for (word,pos) in v]
	# print v
	v = [predicative(word) for word in v] # ["great", "!", "!"]
	v = count(v)                          # {"great": 1, "!": 1}
	return v

print 'TRAINING:'
for title, content, newspaper in csv('newspaper_items.csv'):
	content = title + '\n ' + content
	v = Document(content, type=newspaper, stopwords=False, stemmer=LEMMA)
	# v = instance(content)
	classifier.train(v, newspaper)
	# print newspaper, title

print 'CLASSES:',classifier.classes
print 'terms in classifier:', len(classifier.terms)

import os
files = os.listdir('speeches')
for f in files:
	print f
	s = open(os.path.join('speeches', f)).read().replace('\n','')
	s = plaintext(s)
	d = Document(s, stopwords=False, stemmer=LEMMA)
	# d = instance(content)
	print '\t', classifier.classify(d, discrete=True)

